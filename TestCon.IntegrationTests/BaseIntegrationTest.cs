using Microsoft.Extensions.DependencyInjection;

namespace TestCon.IntegrationTests;

public class BaseIntegrationTest : IClassFixture<IntegrationTestWebAppFactory>, IDisposable
{
    private readonly IServiceScope _scope;    
    protected readonly ApplicationDbContext DbContext;
    protected readonly HttpClient Client;
    
    protected BaseIntegrationTest(IntegrationTestWebAppFactory factory)
    {
        _scope = factory.Services.CreateScope();

        Client = factory.CreateDefaultClient();
        DbContext = _scope.ServiceProvider.GetRequiredService<ApplicationDbContext>();
    }
    
    public void Dispose()
    {
        _scope?.Dispose();
        DbContext?.Dispose();
    }
}