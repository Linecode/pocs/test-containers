using System.Net;
using System.Text;
using FluentAssertions;
using Newtonsoft.Json;

namespace TestCon.IntegrationTests;

public class ExampleTests : BaseIntegrationTest
{
    public ExampleTests(IntegrationTestWebAppFactory factory) : base(factory)
    {
    }

    [Fact]
    public async Task Get_Weather()
    {
        var url = "/weatherforecast";
        var response = await Client.GetAsync(url);
        
        response.StatusCode.Should().Be(HttpStatusCode.OK);
        
        var content = await response.Content.ReadAsStringAsync();
        content.Should().NotBeNullOrWhiteSpace();
    }

    [Fact]
    public async Task Create_And_Get_Todo()
    {
        var body = """
                   {
                     "id": 1,
                     "title": "test",
                     "description": "description"
                   }
                   """;
        var url = "/todo";
        
        var response = await Client.PostAsync(url, new StringContent(body, Encoding.UTF8, "application/json"));
        
        response.StatusCode.Should().Be(HttpStatusCode.OK);
        
        url = "/todo/1";
        
        response = await Client.GetAsync(url);
        
        response.StatusCode.Should().Be(HttpStatusCode.OK);
        
        var content = await response.Content.ReadAsStringAsync();
        content.Should().NotBeNullOrWhiteSpace();
        
        var todo = JsonConvert.DeserializeObject<Todo>(content);
        todo.Should().NotBeNull();
    }
    
    private record Todo(int Id, string Title, string Description);

    [Fact]
    public async Task Create_Bucket()
    {
        var url = "/bucket";
        var response = await Client.PostAsync(url, null);
        
        response.StatusCode.Should().Be(HttpStatusCode.OK);
        var content = await response.Content.ReadAsStringAsync();
        content.Should().NotBeNullOrWhiteSpace()
            .And.Be("true");
    }
}