using System.Net;
using System.Net.Sockets;
using Amazon.S3;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.AspNetCore.TestHost;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Testcontainers.LocalStack;
using Testcontainers.PostgreSql;

namespace TestCon.IntegrationTests;

public class IntegrationTestWebAppFactory : WebApplicationFactory<Program>, IAsyncLifetime
{
    private readonly PostgreSqlContainer _dbContainer = new PostgreSqlBuilder()
        .WithImage("postgres:14.7")
        .WithDatabase("testCon")
        .WithUsername("postgres")
        .WithPassword("postgres")
        .WithCleanUp(true)
        .Build();
    
    private readonly LocalStackContainer _localStack = new LocalStackBuilder()
        .Build();

    protected override void ConfigureWebHost(IWebHostBuilder builder)
    {
        builder.ConfigureTestServices(services =>
        {
            var descriptor =
                services.SingleOrDefault(s => s.ServiceType == typeof(DbContextOptions<ApplicationDbContext>));

            if (descriptor is not null)
                services.Remove(descriptor);

            services.AddDbContext<ApplicationDbContext>(options =>
                options.UseNpgsql(_dbContainer.GetConnectionString()));

            var awsClientDescription = services.SingleOrDefault(s => s.ServiceType == typeof(AmazonS3Client));
            
            if (awsClientDescription is not null)
                services.Remove(awsClientDescription);

            var config = new AmazonS3Config();
            config.ServiceURL = _localStack.GetConnectionString();

            services.AddSingleton(new AmazonS3Client(config));
        });
    }

    public async Task InitializeAsync()
    {
        await _dbContainer.StartAsync();
        await _localStack.StartAsync();
    }

    public new async Task DisposeAsync()
    {
        await _dbContainer.StopAsync();
        await _localStack.StopAsync();
    }

    private static ushort GetNextTcpPort()
    {
        using (var socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp))
        {
            socket.Bind(new IPEndPoint(IPAddress.Loopback, 0));
            return (ushort)(((IPEndPoint)socket.LocalEndPoint!)).Port;
        }
    }
}