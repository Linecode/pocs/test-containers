using Microsoft.EntityFrameworkCore;

namespace TestCon;

public class Todo
{
    public int Id { get; set; }
    public string Title { get; set; }
    public string Description { get; set; }
}

public class ApplicationDbContext : DbContext
{
    public DbSet<Todo> Todos { get; set; } = null!;

    public ApplicationDbContext(DbContextOptions options) : base(options)
    {
        
    }
}